angular.module('app.routes', [])

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider


            .state('menu.home', {
                url: '/page1',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/home.html',
                        controller: 'homeCtrl'
                    }
                }
            })

            .state('menu.cCDetail', {
                url: '/page8',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/cCDetail.html',
                        controller: 'cCDetailCtrl'
                    }
                },
                params:{
                    obj: null
                }
            })

            .state('menu.information', {
                url: '/page9',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/information.html',
                        controller: 'informationCtrl'
                    }
                }
            })

            .state('menu', {
                url: '/side-menu21',
                templateUrl: 'templates/menu.html',
                controller: 'menuCtrl'
            })

            .state('menu.map', {
                url: '/page4',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/map.html',
                        controller: 'mapCtrl'
                    }
                }
            })

            .state('menu.advanceSearch', {
                url: '/page6',
                views: {
                    'side-menu21': {
                        templateUrl: 'templates/advanceSearch.html',
                        controller: 'advanceSearchCtrl'
                    }
                }
            })

        $urlRouterProvider.otherwise('/side-menu21/page1')


    });
