angular.module('app.controllers', [])

    .controller('homeCtrl', ['$scope', '$stateParams', 'StorageService', '$state', 'config', '$filter', '$rootScope', '$http', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, StorageService, $state, config, $filter, $rootScope, $ionicScrollDelegate, $http) {

            $scope.statusFunc = function (list) {
                if ($rootScope.statusFilter) {
                    return (list.summary.cc_status == $rootScope.statusFilter);
                } else {
                    return (list.summary.cc_status);
                }
            };

            $scope.rentalMinFunc = function (list) {
                if ($rootScope.rentalMinFilter) {
                    return (list.summary.cc_rental_min >= $rootScope.rentalMinFilter);
                } else {
                    return (list);
                }
            };

            $scope.rentalMaxFunc = function (list) {
                if ($rootScope.rentalMaxFilter) {
                    return (list.summary.cc_rental_max <= $rootScope.rentalMaxFilter);
                } else {
                    return (list);
                }
            };

            $scope.spaceFunc = function (list) {
                if ($rootScope.spaceMinFilter != null && $rootScope.spaceMaxFilter != null) {
                    return (list.summary.cc_space > $rootScope.spaceMinFilter && list.summary.cc_space < $rootScope.spaceMaxFilter);
                } else {
                    $rootScope.spaceMinFilter = null;
                    $rootScope.spaceMaxFilter = null;
                    return (list);
                }
            }

            $scope.stateOnly = function (list) {
                if ($rootScope.byState) {
                    return (list.state == $rootScope.byState);
                } else {
                    return (list.state);
                }
            }

            //init
            $scope.url = config.url;
            $scope.tabtype = 'group';
            $scope.search = [];
            loadData();

            $scope.doRefresh = function () {
                loadserver();

                $scope.$broadcast('scroll.refreshComplete');
            }

            $scope.gotopage = function (list) {
                $state.go('menu.cCDetail', {
                    obj: list
                });
            }

            $scope.groupname = function (item) {
                $scope.curChar = item;
                return item;
            }

            $scope.tab = function (tabIndex) {
                if (tabIndex == 1) {
                    $scope.tabtype = 'group';
                } else if (tabIndex == 2) {
                    $scope.tabtype = 'state';
                } else if (tabIndex == 3) {
                    $scope.tabtype = 'date';
                }
            }

            $scope.clearsearchinput = function () {
                $scope.search.summary.cc_name = '';
            }

            $scope.clearsearch = function (item) {

                if (item == 'keyword') {
                    $rootScope.keywordFilter = null;
                }

                if (item == 'state') {
                    $rootScope.byState = null;
                }

                if (item == 'status') {
                    $rootScope.statusFilter = null;
                }

                if (item == 'rentalmin') {
                    $rootScope.rentalMinFilter = null;
                }

                if (item == 'rentalmax') {
                    $rootScope.rentalMaxFilter = null;
                }

                if (item == 'space') {
                    $rootScope.spaceMinFilter = null;
                    $rootScope.spaceMaxFilter = null;
                }

            }

            function loadData(item) {
                $rootScope.lists = StorageService.getAll();
            }

            function loadserver() {
                $rootScope.reload();
            }

}])

    .controller('cCDetailCtrl', ['$scope', '$stateParams', 'config', '$ionicScrollDelegate','$ionicModal', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, config, $ionicScrollDelegate, $ionicModal) {
            $scope.cc = $stateParams.obj;
            $scope.url = config.url;

            $scope.isItemShow = [];

            $scope.initShown = function (item, status) {
                $scope.isItemShow[item] = status;
                $scope.$watch("isItemShow." + item, function (val, oldVal) {
                    $ionicScrollDelegate.resize();
                });
            }

            $scope.toggleItem = function (item) {
                if ($scope.isItemShow[item] == true) {
                    $scope.isItemShow[item] = false;
                } else {
                    $scope.isItemShow[item] = true;
                }
            };

            $scope.viewItem = function (item) {
                if ($scope.isItemShow[item] == true) {
                    //$ionicScrollDelegate.resize();
                    setTimeout(function () {
                        $ionicScrollDelegate.resize();
                    }, 150);
                    return 'isshow';

                } else {
                    //$ionicScrollDelegate.resize();
                    setTimeout(function () {
                        $ionicScrollDelegate.resize();
                    }, 150);
                    return 'noshow';
                }
            }

            $scope.showImages = function (image) {
                $scope.modal_image = image;
                $ionicModal.fromTemplateUrl('templates/image-popover.html', {
                    scope: $scope,
                    animation: 'scale-in'
                }).then(function (modal) {
                    $scope.modal = modal;
                    $scope.modal.show();
                });
            }

            $scope.showModal = function (templateUrl) {

            }

            // Close the modal
            $scope.closeModal = function () {
                $scope.modal.hide();
                $scope.modal.remove()
            };


}])

    .controller('informationCtrl', ['$scope', '$stateParams', 'config', '$ionicScrollDelegate', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, config, $ionicScrollDelegate) {

            $scope.isItemShow = [];

            $scope.initShown = function (item, status) {
                $scope.isItemShow[item] = status;
                $scope.$watch("isItemShow." + item, function (val, oldVal) {
                    $ionicScrollDelegate.resize();
                });
            }

            $scope.toggleItem = function (item) {
                if ($scope.isItemShow[item] == true) {
                    $scope.isItemShow[item] = false;
                } else {
                    $scope.isItemShow[item] = true;
                }
            };

            $scope.viewItem = function (item) {
                if ($scope.isItemShow[item] == true) {
                    //$ionicScrollDelegate.resize();
                    setTimeout(function () {
                        $ionicScrollDelegate.resize();
                    }, 150);
                    return 'isshow';

                } else {
                    //$ionicScrollDelegate.resize();
                    setTimeout(function () {
                        $ionicScrollDelegate.resize();
                    }, 150);
                    return 'noshow';
                }
            }

}])

    .controller('menuCtrl', ['$scope', '$stateParams', 'StorageService', '$ionicLoading', 'config', '$http', '$state', '$ionicPopup', 'ionImgCacheSrv', '$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, StorageService, $ionicLoading, config, $http, $state, $ionicPopup, ionImgCacheSrv, $rootScope) {

            $scope.totalCache = function () {
                console.log('load');
                ionImgCacheSrv.getCacheSize()
                    .then(function (result) {
                        var showAlert = $ionicPopup.alert({
                            title: 'Cache size is ' + result + ' bytes',
                        });
                        console.error(result);
                    })
                    .catch(function (err) {
                        console.error(err);
                    })
            }

            $rootScope.reload = function () {
                console.log('fetch from server');
                $ionicLoading.show({
                    template: '<ion-spinner icon="ripple"></ion-spinner><br />Fetching data'
                });

                $http.get(config.apiUrl + 'cc').
                then(function (response) {
                    StorageService.clear();

                    arr = response.data.results;
                    for (var i = 0, len = arr.length; i < len; i++) {

                        $http.get(config.apiUrl + 'cc/' + arr[i].cc_id).
                        then(function (response) {

                            if(response.data.date == null){
                               response.data.date = '-';
                            }else{
                                response.data.date = new Date(response.data.date.replace( /(\d{2})\/(\d{2})\/(\d{4})/, "$1/$2/$3"));
                            }

                            StorageService.add(response.data);
                            if (i == len) {
                                $http.get(config.apiUrl + 'version').
                                then(function (response) {
                                    StorageService.addVersion(response.data.version);
                                    loadData();
                                    $ionicLoading.hide();
                                });
                            }
                        });
                    }
                    //$state.reload();

                }, function errorCallback(response) {
                    $ionicLoading.hide();
                });
            }

            function loadData(item) {
                $rootScope.lists = StorageService.getAll();
            }

}])

    .controller('mapCtrl', ['$scope', '$stateParams', '$rootScope', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope, $state) {

            $scope.gostate = function (item) {
                $rootScope.byState = item;
                //        $state.go('menu.home', {}, {
                //          reload: true, inherit: false, notify: true
                //        });

                $state.transitionTo('menu.home');
            }

}])

    .controller('advanceSearchCtrl', ['$scope', '$stateParams', '$rootScope', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $rootScope, $state) {

            $scope.findnow = function (keyword, status, msc, rentalmin, rentalmax, spacemin, spacemax) {
                $rootScope.keywordFilter = keyword;
                $rootScope.statusFilter = status;
                $rootScope.mscFilter = null;
                $rootScope.rentalMinFilter = rentalmin;
                $rootScope.rentalMaxFilter = rentalmax;
                $rootScope.spaceMinFilter = spacemin;
                $rootScope.spaceMaxFilter = spacemax;
                $state.go('menu.home');
            }

}])
