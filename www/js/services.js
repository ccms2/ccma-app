angular.module('app.services', [])

.factory('StorageService', function ($localStorage) {

    init();

    var _addVersion = function(v){
        $localStorage.version = v;
    }

    var _version = function () {
        return $localStorage.version;
    }

    var _add = function (item) {
      $localStorage.items.push(item);
    }

    var _getAll = function () {
      return $localStorage.items;
    }

    var _clear = function (){
        $localStorage.$reset();
        init();
    };

    function init(){
        $localStorage = $localStorage.$default({
          items: []
        });
    }

    return {
        getAll: _getAll,
        add: _add,
        version: _version,
        addVersion: _addVersion,
        clear: _clear,
    };
})

.factory('BlankFactory', [function () {

}])

.service('BlankService', [function () {

}]);
