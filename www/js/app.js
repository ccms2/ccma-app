// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.directives', 'app.services', 'ngStorage', 'angular.filter', 'ionic', 'ionicImgCache'])

    .config(function ($ionicConfigProvider, $sceDelegateProvider, ionicImgCacheProvider) {

        $sceDelegateProvider.resourceUrlWhitelist(['self', '*://www.youtube.com/**', '*://player.vimeo.com/video/**']);
        // Remove back button text completely
        $ionicConfigProvider.backButton.previousTitleText(false).text('');

        // Set storage size quota to 100 MB.
        ionicImgCacheProvider.quota(100);
        // Set cache clear limit.
        ionicImgCacheProvider.cacheClearSize(100);

    })

    .run(function ($ionicPlatform, StorageService, $ionicLoading, config, $http, $state, $ionicPopup, $ionicHistory) {
        $ionicPlatform.ready(function () {

            window.onerror = function (errorMsg, url, lineNumber) {
                 alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber);
            }
//            setTimeout(function() {
//                navigator.splashscreen.hide();
//            }, 300);

//            ionic.Platform.fullScreen();
            // Disable BACK button on home
//            Splashscreen.hide();
            $ionicPlatform.registerBackButtonAction(function(event) {
                if ($state.current.name=="menu.home") { // your check here
                  $ionicPopup.confirm({
                    title: 'Exit application',
                    template: 'Are you sure you want to exit?'
                  }).then(function(res) {
                    if (res) {
                      ionic.Platform.exitApp();
                    }
                  })
                }else{
                    $ionicHistory.goBack();
                }
            }, 100);

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }

//            if(StorageService.version() == null){
//                init();
//             }else{
//                $http.get(config.apiUrl+'version').
//                    then(function(response) {
//                        if(StorageService.version() != response.data.version){
//                            init();
//                            StorageService.addVersion(response.data.version);
//                        }
//                    },  function errorCallback(response) {
//                        $ionicLoading.hide();
//                    });
//             }

        });


        function init(){
            console.log('fetch from server');
            StorageService.clear();
            $ionicLoading.show({
                template: '<ion-spinner icon="ripple"></ion-spinner><br />Fetching data'
            });

            $http.get(config.apiUrl+'cc').
                then(function(response) {

                    arr = response.data.results;
                    for (var i = 0, len = arr.length; i < len; i++) {

                        $http.get(config.apiUrl+'cc/'+arr[i].cc_id).
                            then(function(response) {
                                StorageService.add(response.data);
                                if(i == len){
                                    $http.get(config.apiUrl + 'version').
                                    then(function (response) {
                                        StorageService.addVersion(response.data.version);
                                        $ionicLoading.hide();
                                    });
                                }
                            });
                    }

                    //$state.reload();

                },  function errorCallback(response) {
                    $ionicLoading.hide();

                    var showAlert  = $ionicPopup.alert({
                        title: 'Connection Problem',
                        okText: 'Reload',
                    });

                    showAlert.then(function(res) {
                        init();
                    })
            });
        }
    })

    /*
      This directive is used to disable the "drag to open" functionality of the Side-Menu
      when you are dragging a Slider component.
    */
    .directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function ($ionicSideMenuDelegate, $rootScope) {
        return {
            restrict: "A",
            controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {

                function stopDrag() {
                    $ionicSideMenuDelegate.canDragContent(false);
                }

                function allowDrag() {
                    $ionicSideMenuDelegate.canDragContent(true);
                }

                $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
                $element.on('touchstart', stopDrag);
                $element.on('touchend', allowDrag);
                $element.on('mousedown', stopDrag);
                $element.on('mouseup', allowDrag);

        }]
        };
}])

    /*
      This directive is used to open regular and dynamic href links inside of inappbrowser.
    */
    .directive('hrefInappbrowser', function () {
        return {
            restrict: 'A',
            replace: false,
            transclude: false,
            link: function (scope, element, attrs) {
                var href = attrs['hrefInappbrowser'];

                attrs.$observe('hrefInappbrowser', function (val) {
                    href = val;
                });

                element.bind('click', function (event) {

                    window.open(href, '_system', 'location=yes');

                    event.preventDefault();
                    event.stopPropagation();

                });
            }
        };
    })


    .constant('config', {
        appName: 'My App',
        appVersion: 2.0,
        apiUrl:'https://ccms.mdec.com.my/api/',
        url: 'https://ccms.mdec.com.my'
//        apiUrl:'https://13.76.174.220/api/',
//        url: 'https://13.76.174.220'
//        apiUrl:'http://192.168.0.101:8000/api/',
//        url: 'http://192.168.0.101:8000'
    });

